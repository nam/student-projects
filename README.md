## deutsch:
Dies ist das gitlab-Projekt für die Sammlung von Themen und Beschreibungen für mögliche Abschlussarbeiten.
Die Beschreibungen finden sie im [Wiki](https://gitlab.gwdg.de/nam/student-projects/-/wikis). Das git-Repository dient lediglich dafür weitere Unterlagen zur Verfügung stellen zu können. 

## english:
This is the gitlab project for the collection of topics and descriptions of possible theses projects. 
The descriptions are stored in the [Wiki](https://gitlab.gwdg.de/nam/student-projects/-/wikis). 
The git repository only serves the purpose of allowing to provide additional material.


## Short cut: 
If you want to directly skip to one of the research groups of the Institute for Numerical and Applied Mathematics, select the work group here:
At the institute for Numerical and Applied Math we have six work groups. Following the corresponding link below will lead you to descriptions of possible topics for bachelor or master theses or practicals. You may also contact the corresponding professors directly.

- [Inverse Problems (T. Hohage)](https://gitlab.gwdg.de/nam/student-projects/-/wikis/Hohage), see also [work group homepage](https://www.uni-goettingen.de/de/87248.html)
- [Computational partial differential equations (C. Lehrenfeld)](https://gitlab.gwdg.de/nam/student-projects/-/wikis/Lehrenfeld), see also [work group homepage](https://cpde.math.uni-goettingen.de/)
- [Continuous optimization and variational analysis (R. Luke)](https://gitlab.gwdg.de/nam/student-projects/-/wikis/Luke), see also [work group homepage](http://vaopt.math.uni-goettingen.de/en)
- [Mathematical signal and image processing (G. Plonka-Hoch)](https://gitlab.gwdg.de/nam/student-projects/-/wikis/Plonka-Hoch), see also [work group homepage](https://www.uni-goettingen.de/de/206699.html)
- [Applied mathematics in the natural sciences (A. Wald)](https://gitlab.gwdg.de/nam/student-projects/-/wikis/Wald)
- [Discrete differential Geometry (M. Wardetzky)](https://gitlab.gwdg.de/nam/student-projects/-/wikis/Wardetzky), see also [work group homepage](https://www.uni-goettingen.de/de/90335.html)

